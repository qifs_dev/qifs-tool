#ifndef UTILS_H_   /* Include guard */
#define UTILS_H_

#include <stdio.h>
#include "string.h"
#include "list.h"
#include "mathlink.h"
#include "config.h"

/*
This is the data structure to represent the bounds of variables
 */
typedef struct bounds {
   char* name;
   char* lowerBound;
   char* upperBound;
   //the list of strings that this variable doesn't contain
   stringList* not_contains;

   //the list of regex that this variable doesn't match
   stringList* not_match;

   //the list of string that this variable doesn't equal
   stringList* not_equal;

   //has content operator
   int content_opr;   

   //get zero_lower_bound
   int zero_lower_bound;

   //concat list
   intList* concat_list;

   //number of elements in concat_list;
   int nconcat;

   //no. of not contains
   int nc;
   //no. of not match regex
   int nm;
   //no. of not equal string
   int ne;
   //lower bound of length
   int lenL;
   //upper bound of length
   int lenU;
} bounds;

void initialize(bounds* b, int n);

/*
Send request expression, normalize it 
 */

void send_to_Mathematica(MLINK lp, char *expression, char** result);

/*
Calculate MINF function
 */
void MINF(MLINK lp, char* expresison1, char* expression2, char** result);

/*
Calculate MAXF function
 */
void MAXF(MLINK lp, char* expression1, char* expression2, char** result);


/*
Calculate MAXF function
 */
void SUBF(MLINK lp, char* poly, int number, char** result);

/*
Calculate F1
 */
void F1(MLINK lp, char* P, char** result);

/*
Calculate other operator like +, -, *, /
 */
void Calc(MLINK lp, char* expression1, char* expression2, char** result, char opr, int M);

/*
Calculate not operator
 */
char* NOT(MLINK lp, bounds input, bounds* results, int M);

/*
Calculate the coefficient a_number
 */
void get_coef(MLINK lp, char* expression, int number, char** result);

/*
Calculate the conjunction between two conditions
 */
void AND(MLINK lp, bounds input1, bounds input2, bounds* results);


/*
Calculate the disjunction between two conditions
 */
void OR(MLINK lp, bounds input1, bounds input2, bounds* results, int M);

/*
Get lower bound for res = p1.p2
 */
void optimize_concat(MLINK lp, char* p1, char* p2, char** res, int M);
/*
Copy lower bound and upper bound, other fields of results keep the same
 */
void copy_bounds(bounds input, bounds* results);

/*
Copy lower bound and upper bound from strings, other fields of results keep the same
 */
void copy_bounds_concrete(char* lower, char* upper, bounds* results);

/*
Compute S != S1, S != S2, ..., S != Sn
 */
void compute_n_not_equal(MLINK lp, char** s, int n, bounds* result, int M);

/*
Compute S = S1 or S = S2 or ... or S = Sn
 */
void compute_or_n_equal(MLINK lp, char** s, int n, bounds* result, int M);
/*
switch the operator
 */
void switch_operator(char** op);

/**
 * get the max value of two numbers
 * @param  x  first number
 * @param  y second number
 * @return   max value
 */
int max(int x, int y);

int min(int x, int y);

/**
 * get absolute value of number x
 * @param  x [the number]
 * @return   [the absoulute value of x]
 */
double get_abs(double x);

/*
This function builds the auto correlation table of string s
and stores the result into the table ctable
*/
void compute_correlation_table(char* s, int ctable[], int size);


/*
This function builds the reverse correlation table of string s
and stores the result into the table reverse_ctable
*/
void compute_reverse_correlation_table(char* s, int reverse_ctable[], int size);


/*
The aim of preprocess are to get the bounds of length for all variables 
and decide which variable should have the lower bound of 0
 */
void preprocess(MLINK lp, FILE *file, bounds var[], int nVar);

void make_simple_call(MLINK lp);
#endif

