#ifndef MATCH_REGEX_H_   /* Include guard */
#define MATCH_REGEX_H_

#include "utils.h"
#include "mathlink.h"

/*
This function counts the GFs for bounds of strings matching ther regular expression
 */
void count_match_regex(MLINK lp, char* regular_expression, struct bounds* results, int M);

/*
This function counts the GFs for bounds of strings not matching the regular expression
 */
void count_not_match_regex(MLINK lp, char* regular_expression, struct bounds* results, int M);


/*
This function returns the GF of bounds for \neg contains(S, s_i)
*/
void count_n_not_match(MLINK lp, char** s, int n, struct bounds* results, int alphabetSize);

#endif