all: main_qifs
main_qifs: utils.o contains.o match_regex.o list.o main.o
	gcc -o main_qifs main.o list.o utils.o contains.o match_regex.o -lML64i3 -lm -lpthread -lrt -lstdc++  

list.o: list.c
	gcc -c list.c

utils.o: utils.c
	gcc -c utils.c

contains.o: contains.c
	gcc -c contains.c

match_regex.o: match_regex.c
	gcc -c match_regex.c

main.o: main.c
	gcc -c main.c

clean:
	\rm *.o
	\rm main_qifs
