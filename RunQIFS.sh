#!/bin/bash
# Author : Loi Luu
if (( "$#" != 1 ))
then
    echo "Usage info:..."
    echo "./RunQIFS <input_file>"
    echo "for example, run as: ./RunQIFS input_qifs.txt    ."
exit 1
fi
echo "Calling the parser..."
python qifs_parser.py "$1";
echo "Calling the counter..."
./main_qifs inter_input.txt
echo "Finished. Please check the result in output.txt"
rm inter_input.txt;
#remove the python  generated file
rm -f *.pyc;
rm -f parsetab.py;
rm -f parser.out;
