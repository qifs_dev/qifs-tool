#ifndef CONFIG_H_   /* Include guard */
#define CONFIG_H_

//maximum length of input
#define MAX_GF 630

//maximum of regex len
#define MAX_REGEX_LEN 1000

//maximum length of const string or pattern
#define MAX_PATTERN_LEN 100

//NUmber of characters in space
#define alphabet_size 256

//concat optimize option
#define OPT_CONCAT_OPTIMIZE 1

//Number of interator for concat, the greater the better (but slower)
#define CONCAT_ITERS 5

//code of intermediate representation of constraint
#define CODE_NEW_VAR 0
#define CODE_NOT 1
#define CODE_OR 2
#define CODE_CONTAINS 3
#define CODE_STRSTR 4
#define CODE_EQUAL 5
#define CODE_CONCAT 6
#define CODE_LENGTH 7
#define CODE_REGEX 8
#define CODE_EQUAL_CSTR 9
#define CODE_ALL_STR 10
#define CODE_QUERY 11

#endif
